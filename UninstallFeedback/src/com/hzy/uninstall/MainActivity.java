package com.hzy.uninstall;

import com.hzy.uninstall.R.id;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity implements OnClickListener {

	private Button btnRegisterLocal;
	private Button btnKill;
	private Button btnExit;
	private Button btnRegisterRemote;
	private Button btnUninstall;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		btnRegisterLocal = (Button) findViewById(id.button_register_local);
		btnRegisterRemote = (Button) findViewById(id.button_register_remote);
		btnKill = (Button) findViewById(id.button_kill);
		btnUninstall = (Button) findViewById(id.button_uninstall);
		btnExit = (Button) findViewById(id.button_exit);

		btnRegisterLocal.setOnClickListener(this);
		btnRegisterRemote.setOnClickListener(this);
		btnKill.setOnClickListener(this);
		btnUninstall.setOnClickListener(this);
		btnExit.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case id.button_register_local:
			UninstallCallback.getInstance(this).start(true);
			break;
			
		case id.button_register_remote:
			UninstallCallback.getInstance(this).start(false);
			break;

		case id.button_kill:
			UninstallCallback.getInstance(this).stop();
			break;
			
		case id.button_uninstall:
			showUninstallPage();
			break;
			
		case id.button_exit:
			finish();
			break;

		default:
			break;
		}
	}

	private void showUninstallPage() {
		String pkgName = getPackageName();
		Uri uri = Uri.parse("package:" + pkgName);
		Intent i = new Intent();
		i.setAction(Intent.ACTION_DELETE);
		i.setData(uri);
		startActivity(i);
	}
}
