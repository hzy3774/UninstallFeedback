package com.hzy.uninstall;

public class UninstallConsts {
	public static final String OBSERVED_FILE = "uninstall_observed";
	public static final String LOCK_FILE = "uninstall_lock";
	public static final String REMOTE_URL = "http://hzy3774.iteye.com/";
	public static final String EXTERNAL_APP_PATH = "uninstall";
	public static final String LOCAL_FILE_URL_HEADER = "file://";
	public static final String LOCAL_PAGE_NAME = "local_page.html";
}
