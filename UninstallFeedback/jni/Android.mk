LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := uninstall
LOCAL_SRC_FILES := uninstall.cpp

LOCAL_CFLAGS += -DLOG_ENABLE
LOCAL_LDLIBS := -llog
include $(BUILD_SHARED_LIBRARY)
