# UninstallFeedback
Auto launch a feedback webpage after uninstall the app on Android
####Android实现卸载App后弹出指定网页
* 网页内容可以指定为本地html文件，也可以是远程URL
* 通过监听App的目录下的文件实现，卸载时系统会自动清空这些目录
